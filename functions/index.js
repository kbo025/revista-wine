const functions = require('firebase-functions');
const cors = require('cors')({ origin: true })
const admin = require('firebase-admin')
admin.initializeApp()

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions
exports.webhook = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
      let event;
  
      try {
        event = JSON.parse(request.body);
      } catch (err) {
        response.status(400).send(`Webhook Error: ${err.message}`);
      }
  
      let data = event.data.object;
      let db = admin.database();
      let ref = db.ref("test");
  
      ref.set(data);
  
  
      
  
    // Handle the event
    // switch (event.type) {
    //   case 'payment_intent.succeeded':
    //     const paymentIntent = event.data.object;
    //     // Then define and call a method to handle the successful payment intent.
    //     // handlePaymentIntentSucceeded(paymentIntent);
    //     break;
    //   case 'payment_method.attached':
    //     const paymentMethod = event.data.object;
    //     // Then define and call a method to handle the successful attachment of a PaymentMethod.
    //     // handlePaymentMethodAttached(paymentMethod);
    //     break;
    //   // ... handle other event types
    //   default:
    //     // Unexpected event type
    //     return response.status(400).end();
    // }
  
    })
  })
  